﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using DavosOilFinder.Models;
using DavosOilFinder.Data;
using System.Dynamic;
using System.IO;
using CsvHelper;
using System.Globalization;
using DavosOilFinder.Data.Davos;

namespace DavosOilFinder.Controllers
{
    public class HomeController : Controller
    {

        private readonly DavosContext _davosDbContext;

        public HomeController(DavosContext davosDbContext)
        {
            _davosDbContext = davosDbContext;
        }

        public IActionResult Index()
        {
            return View();
        }


    }
}
