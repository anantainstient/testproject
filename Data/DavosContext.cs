using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using DavosOilFinder.Data.Davos;


namespace DavosOilFinder.Data
{
    public class DavosContext : DbContext
    {
        protected readonly DbContextOptions _options;
        protected ModelBuilder modelBuilder;
        public static bool IsTestMode = false;
        public DavosContext(DbContextOptions options) : base(options)
        {
            _options = options;
        }
        public DbSet<Davos.Customer> customers { get; set; }
        public DbSet<Davos.Order> orders { get; set; }
    }
}
