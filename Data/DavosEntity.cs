namespace DavosOilFinder.Data.Davos
{
    public class Customer
    {
        public int Id { get; set; }
        public string name { get; set; }
        public string country { get; set; }
    }

    public class Order 
    {
        public int Id { get; set; }
        public int customer_id { get; set; }
        public decimal amount { get; set; }
    }

}
